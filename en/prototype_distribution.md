---
title: Distribution of prototypes
---

# Distribution of prototypes 


**Hello fellow researcher,**

so you want to become part of the card10 research team and help us find out how it will have improved everyone's experience at Chaos Communication Camp 2019?
Here's what you have to know:
* we have around 20 prototypes which we are trying to share with as many interested card10logists as possible. and there are many people interested in working with it.
* there's documentation in our [wiki](https://gitlab.hamburg.ccc.de/card10/logix/)
* we're trying to find out how the badge will have interacted (?) with used BLE to interface with other installations at camp the camp surroundings
* cities with hackspaces can get a prototype if there's a volunteer for being an ambassador who takes care of the prototype (trying to avoid too much idle time)


### Here's what you can do:
* think about how you want to contribute/what you want to do with the prototype
* join our [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)):( [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr))/[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) ( [`freenode.com#card10badge`](ircs://chat.freenode.net:6697/card10badge))
channel and tell us what you want to do
* talk to your city's ambassador
* become an ambassador yourself
* we'll try to find a timeslot for you to work with the prototype
* we send it to you and after the time we agreed upon you send it back to us/the next person


### How to be a great card10logy department ambassador:
* get more people involved to help with 'ambassadoring' ;P
* invite others to chat about the card10
* organise meetups to play with card10
* invite related groups (wagenburg, fab labs, ...)
* contact and encourage people who might have great ideas but are not aware of the project/too shy to ask
* share your research in the card10logix wiki, matrix/irc and @card10badge

## Cities that already have ambassadors
* Berlin: 2 prototypes, bi-weekly meeting at the [xHain](https://x-hain.de); ambassador: miriamino contact via matrix/irc chat)
* Munich
* Bremen: location: ccchb; ambassador: lilafisch (contact via card10 matrix/irc chat, in [ccchb irc](http://ccchb.de/wiki/Chat))